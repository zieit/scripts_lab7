import {Component, OnInit} from '@angular/core'
import {ActivatedRoute} from "@angular/router";
import {Post, PostsService} from "../posts.service";

@Component
({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})

export class PostComponent implements OnInit
{
  post?: Post;
  posts: Post[] = [];
  constructor(private activatedRouter: ActivatedRoute, private postS: PostsService, private router: Router,) {}
  ngOnInit(): void 
  {
    this.activatedRouter.params.subscribe((param)=>{this.post = this.postS.getById(+param['id']!)})
  }
  goBack() {
    console.log('go back');
    this.router.navigate(['/posts']);
  }

  loadPost() {
    this.posts = this.postS.posts
  }
}
